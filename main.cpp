/* Constexpr lookup generator */
/* 2020 by Stephan Roslen */

#include <algorithm>
#include <array>
#include <iostream>
#include <string_view>

#include <fmt/core.h>

template<size_t n, typename Gen, size_t... iseq>
constexpr auto generateArray(Gen &&gen, const std::index_sequence<iseq...>)
    -> std::array<std::invoke_result_t<std::decay_t<Gen>, size_t>, n> {
  return {gen(iseq)...};
}

template<size_t n, typename Gen>
constexpr auto generateArray(Gen &&gen) {
  return generateArray<n>(std::forward<Gen>(gen),
                          std::make_index_sequence<n>());
}

template<typename T, size_t n, typename Comp>
constexpr std::array<T, n> generateArrayPullMin(const std::array<T, n> arr,
                                                const size_t p, Comp &&comp) {
  size_t idxm{p};
  for (size_t i{p + 1}; i < n; ++i) {
    if (comp(arr[i], arr[idxm])) { idxm = i; }
  }
  return generateArray<n>([idxm, p, arr](size_t idx) -> T {
    if (idx == idxm) { return arr[p]; }
    if (idx == p) { return arr[idxm]; }
    return arr[idx];
  });
}

template<typename T, size_t n, typename Comp>
constexpr std::array<T, n> generateArraySortedUpTo(const std::array<T, n> arr,
                                                   size_t p, Comp &&comp) {
  auto tmp{p > 0 ? generateArraySortedUpTo(arr, p - 1, comp) : arr};
  return generateArrayPullMin(tmp, p, std::forward<Comp>(comp));
}

template<typename T, typename Comp>
constexpr T generateArraySorted(const T arr, Comp &&comp);

template<typename T, size_t n, typename Comp>
constexpr std::array<T, n> generateArraySorted(const std::array<T, n> arr,
                                               Comp &&comp) {
  return generateArraySortedUpTo(arr, n - 1, std::forward<Comp>(comp));
}

template<size_t col>
constexpr auto createColumnComparator(void) {
  return [](const auto &a, const auto &b) -> bool {
    return std::get<col>(a) < std::get<col>(b);
  };
}

template<size_t colIndex, size_t colRef, typename T, size_t n>
constexpr auto createLookup(const std::array<T, n> &arr) {
  const auto comp = createColumnComparator<colIndex>();
  const auto sorted = generateArraySorted(arr, comp);
  using RowType = T;
  using LookupType = std::tuple_element_t<colIndex, RowType>;
  using ResultType = std::tuple_element_t<colRef, RowType>;
  return [sorted](const LookupType &input) -> ResultType {
    auto rangeFirst{std::begin(sorted)};
    auto rangeLast{std::end(sorted)};
    auto rangeLength{std::distance(rangeFirst, rangeLast)};

    while (rangeLength > 0) {
      auto it{rangeFirst};
      auto step = rangeLength / 2;
      std::advance(it, step);
      if (std::get<colIndex>(*it) < input) {
        rangeFirst = ++it;
        rangeLength -= step + 1;
      } else {
        rangeLength = step;
      }
    }
    decltype(auto) lookupVal = rangeFirst != rangeLast
        ? std::get<colIndex>(*rangeFirst)
        : throw std::runtime_error("Value not found");
    bool found = lookupVal == input;
    return found ? std::get<colRef>(*rangeFirst)
                 : throw std::runtime_error("Value not found");
  };
};

int main() {
  constexpr std::array<std::tuple<size_t, std::string_view, size_t>, 4> data{
      {{0, "Doctor Who", 12},
       {1, "Battlestar Galactica", 4},
       {2, "Firefly", 1},
       {3, "Babylon 5", 5}}};

  constexpr auto seasonsLookupByName = createLookup<1, 2>(data);

  constexpr auto numFirefly = seasonsLookupByName("Firefly");
  constexpr auto numDoctorWho = seasonsLookupByName("Doctor Who");

  constexpr auto showLookupByIdx = createLookup<0, 1>(data);

  constexpr auto name1 = showLookupByIdx(1);
  constexpr auto name3 = showLookupByIdx(3);

  fmt::print("{}; {}\n", numFirefly, numDoctorWho);
  fmt::print("{}; {}\n", name1, name3);

  return 0;
}
